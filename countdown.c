///////////////////////////////////////////////////////////////////////////////
//          University of Hawaii, College of Engineering
/// @brief  Lab 06c - countdown - EE 205 - Spr 2022
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author Tomoko Austin<tomokoau@hawaii.edu>
// @date   23_feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main(void) {
   printf("Reference time: Tue Jan 21 04:26:07 PM HST 2014");
    time_t cur_time;
    time_t ref_time;
      struct tm * REF_DATE; {
       .tm_year = 2014-1900,
       .tm_mon  = 1-1,
       .tm_mday = 21,
       .tm_hour = 16,
       .tm_min  = 26,
       .tm_sec  = 7,
      };
    ref_time = mktime(&REF_DATE);
    time(&cur_time);
    while(true){
    int y, m, d, hh, mm, ss;
    int diff_t;
    time(&cur_time);
    diff_t=difftime(cur_time, ref_time);
    y  = (diff_t/31536000);
    m  = (diff_t-(3153600*y)/30);
    d  = (diff_t-(3153600*y)-(m*30));
    hh = (diff_t/3600);
    mm = (diff_t-(3600*hh)/60);
    ss = (diff_t-(3600*hh)-(mm*60));

   printf("Years: %d Days: %d Hours: %d Minutes %d Seconds: %d",y,m,d,hh,mm,ss);

       sleep(1);
   
   

    }



//while (true)
   return 0;
}
